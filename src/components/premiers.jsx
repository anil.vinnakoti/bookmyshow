import React, { Component } from 'react'
import { premiereData } from '../premiereData';
import PremiereCard from './premiereCard';

class Premieres extends Component {
  state = {  } 
  render() { 
    return (
      <div className='bg-dark-color1 d-flex flex-column py-4'>
        <div className='container'>
          <div>
            <a href='#'><img className='container w-100' src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-1440,h-120/premiere-rupay-banner-web-collection-202104230555.png" alt="" /></a> 
          </div>

          <div className=' fw-bold text-white my-4'>
            <h5 className=' my-0'>Premiers</h5>
            <p className='my-0' style={{fontSize:'14px'}}>Brand new releases for every Friday</p>
          </div>

          <div className='row px-3'>
            {premiereData.map(item => 
              <PremiereCard key={item.id} item={item}/>                    
            )}
          </div>
        </div>

      </div>
    );
  }
}
 
export default Premieres;