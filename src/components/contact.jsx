import React, { Component } from 'react'

class Contact extends Component {
  state = {  } 
  render() { 
    return (
      <div className='bg-dark-color2 py-2'>
        <div className='container row-sm justify-content-between' >
          <div className='row'>
            <div className=' col d-flex '>
              <img src="https://in.bmscdn.com/webin/common/icons/hut.svg" alt="tent icon" />
              <p className='text-white ms-2 mt-3' >List your Show</p>
            </div>

            <div className='text-white mt-3'>
              Got a show event, activity or a great experience? Partner with us &amp; get listed on BookMyShow
            </div>
          </div>


          <div className='mt-3'>
            <button className='contact px-3 py-1'>Contact today!</button>
          </div>
        </div>
      </div>
    );
  }
}
 
export default Contact;