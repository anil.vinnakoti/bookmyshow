import React, { Component } from 'react'

class PremiereCard extends Component {
  state = {  } 
  render() { 
    const {item} = this.props
    return (
      <div className='card col-md-4 col-sm-6 flex-lg-grow-1 col-lg-2 mb-md-4 mb-3 border-0 bg-dark-color1'>
        <div>
          <a href='#'><img className="card-img-top rounded-3  " src={item.image}  alt="premiere image"/></a>
        </div>
        <div className="card-body bg-dark-color1 px-0 w-75">
          <p className="card-text fw-bold text-white m-0" style={{fontSize:'16px'}}>{item.title}</p>
          <p className="card-text text-white m-0 ">{item.language}</p>
        </div>
      </div>
    );
  }
}
 
export default PremiereCard;