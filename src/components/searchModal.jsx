import React, { Component } from 'react'

class SearchModal extends Component {
  state = {  } 
  render() { 
    return (
      <div>

        <form data-bs-toggle="modal" data-bs-target="#exampleModal" className="d-flex ms-lg-2">
          <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
        </form>

        <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-fullscreen">
            <div className="modal-content">
              <div className="modal-header d-flex ">

                <form  className="d-flex ms-auto">
                  <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                </form>

                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div className="modal-body">
                <div className='container d-flex'>
                  <div className="dropdown">
                    <button className="btn " type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                      MOVIES
                    </button>
                    <ul className="dropdown-menu min-vw-0 width-for-search border-0 ">
                      <div className='d-flex justify-content-between'>
                      <div>
                        <li className="list-group-item p-0  border-0 movie-language"> Kannada
                            <ul className="list-group my-2">
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Fourwalls (UA)</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Ide Antaranga Shuddhi (UA)</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Love Mocktail 2 (U)</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Oppanda (UA)</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Preetigibbaru (UA)</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Rowdy Baby (UA)</a></li>
                            </ul>
                        </li>

                        <li className="list-group-item p-0  border-0 my-4 movie-language"> English
                            <ul className="list-group  my-2">
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Death on the Nile</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Marry Me (UA)</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Moonfall</a></li>
                              <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Spider-Man No way Home </a></li>
                            </ul>
                        </li>
                      </div>
                      
                      <div>
                      <li className="list-group-item p-0  border-0 my-4 movie-language"> Hindi
                          <ul className="list-group my-2">
                            <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>83 (U)</a></li>
                            <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Badhaai Do (UA)</a></li>
                            <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Pushpa: The Rise-Part 01 (Hindi) (UA)</a></li>
                          </ul>
                      </li>

                      <li className="list-group-item p-0  border-0 my-4 movie-language"> Telugu
                          <ul className="list-group my-2">
                            <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>DJ Tillu (UA)</a></li>
                            <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Khiladi (UA)</a></li>
                            <li className="list-group-item p-0 border-0 my-1 movie-title" ><a className='text-decoration-none text-reset' href='#'>Sehari (UA)</a></li>
                          </ul>
                      </li>
                      </div>
                      </div>
 


                      

                    </ul>
                  </div>
                  <div className="dropdown">
                    <button className="btn" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                      CINEMAS
                    </button>
                    <ul className="dropdown-menu border-0 height-for-cinemas">
                      <div className='theatre-container h-100 d-flex flex-column flex-wrap'>
                        <p><a className='text-decoration-none text-reset' href='#'>Abhinay Theatre 4K A/C: Gandhinagar</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Akash Cinemas: Laggere</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Amruth Digital 2K A/C Cinema</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Aruna Theatre A/C 4K 7.1 Dolby Atmos</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Ashoka Theatre</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Bharati Theatre</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Bhumika Digital 2K Cinema</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Cinepolis: BInnypet Mall</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Cauvery Digital 4k Cinema: Sankey Road</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Chandrodaya Digital 2K Cinema: Vidyapeeta Circle</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Cinepolis: Orion East Mall, Banaswadi</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Cinepolis: SJR (Central Mall) Arekere, Bannergatta</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Eshwari Digital 4K Cinema: Banashankari</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Galaxy Paradise (Miniplex): Begur Road</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Ganesh Digital 2K Cinema: Yelahanka New Town</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Gopalan Grand Mall: Old Madras Road</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Gopalan Cinemas: Arcade Mall, Mysore Road</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>INOX: Brookefield Mall</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>INOX: Central, JP Nagar, Mantri Junction</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>INOX: Galleria Mall, Yelahanka</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>INOX: Garuda Swagath Mall, Jayanagar</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Lakshmi 4K Dolby Atmos: Tavarekere</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Kamakya Cinema: Banashankari</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>Mukta A2 Cinemas: Vaishnavi Vaibhavi, Bengaluru</a></p>
                        <p><a className='text-decoration-none text-reset' href='#'>PVR: 4DX, Orion Mall, Dr Rajkumar Road</a></p>

                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="modal-footer">

              </div>
            </div>
          </div>
        </div>
      </div>
        
    );
  }
}
 
export default SearchModal;