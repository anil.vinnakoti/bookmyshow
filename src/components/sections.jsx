import React, { Component } from 'react'
class Sections extends Component {
  state = {  } 
  render() { 
    return (
      <div className='sections navbar  d-flex justify-content-between'>
        <ul  className="list-group d-flex flex-wrap list-group-horizontal">
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Movies</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Stream</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Events</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Play</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Sports</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Activities</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Buzz</a></li>
        </ul >

        <ul className="list-group d-flex flex-wrap list-group-horizontal">
          <li className="list-group-item bg-dark-color4 text-white px-2 "><a className='text-decoration-none text-white' href='#'>ListYouShow</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Corporate</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Offers</a></li>
          <li className="list-group-item bg-dark-color4 text-white px-2"><a className='text-decoration-none text-white' href='#'>Gift Cards</a></li>
        </ul>
      </div>
    );
  }
}
 
export default Sections;