import React, { Component } from 'react'

class BestOfEnterTainment extends Component {
  state = {  } 
  render() { 
    return (
      <div className='container d-flex flex-column my-5'>
        <div className='fs-5 fw-bold'>
          The Best of Entertainment
        </div>

        <div className='row '>
          <div className=' col-md-4 col-sm-6 flex-lg-grow-1 col-lg-2 my-2'>
            <a href='#'><img className='card rounded-3 w-100' src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MTE1KyBFdmVudHM%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/workshops-collection-202007231330.png" alt="category image" /></a>
          </div>

          <div className='col-md-4 col-sm-6 flex-lg-grow-1 col-lg-2 my-2'>
          <a href='#'><img className='card rounded-3 w-100' src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MTAgRXZlbnRz,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/fitness-collection-2020081150.png" alt="category image" /></a>

          </div>

          <div className='col-md-4 col-sm-6 flex-lg-grow-1 col-lg-2 my-2'>
          <a href='#'><img className='card rounded-3 w-100' src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MjArIEV2ZW50cw%3D%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/kids-collection-202007220710.png" alt="category image" /></a>

          </div>

          <div className='col-md-4 col-sm-6 flex-lg-grow-1 col-lg-2 my-2'>
          <a href='#'><img className='card rounded-3 w-100' src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-ODArIEV2ZW50cw%3D%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/comedy-shows-collection-202007220710.png" alt="category image" /></a>

          </div>
          <div className='col-md-4 col-sm-6 flex-lg-grow-1 col-lg-2 my-2'>
          <a href='#'><img className='card rounded-3 w-100' src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-800,h-800:ote-MjArIEV2ZW50cw%3D%3D,otc-FFFFFF,otf-Roboto,ots-64,ox-48,oy-320,ott-b:w-300/music-shows-collection-202007220710.png" alt="category image" /></a>

          </div>

        </div>
      </div>
    );
  }
}
 
export default BestOfEnterTainment;