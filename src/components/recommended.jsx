import React, { Component } from 'react'
import MovieCard from './movieCard';
import {moviesData} from '../moviesData'

class Recommended extends Component {
  state = {  } 
  render() { 
    return (
      <div className='container d-flex flex-column my-4'>
        <div className='fs-5 fw-bold'>
          Recommended Movies
        </div>
        <div className='row px-2 my-2'>
          {moviesData.map(item => 
            <MovieCard key={item.id} item={item}/>                    
          )}
        </div>

        
      </div>
    );
  }
}
 
export default Recommended;