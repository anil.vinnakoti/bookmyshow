import React, { Component } from 'react'
import SearchModal from './searchModal';
import Sections from './sections';

class Navbar extends Component {
  state = {  } 
  render() { 
    return (
          <>
          <nav className="navbar navbar-expand-lg justify-content-center navbar-light">
            <div className="container">
              <img className='bookmyshow-icon' src='https://www.pngitem.com/pimgs/m/193-1936701_transparent-bookmyshow-logo-hd-png-download.png'/>
              <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse justify-content-between" id="navbarSupportedContent">
                <ul className="navbar-nav mb-2 mb-lg-0 order-1">
                <li className="nav-item dropdown ">
                    <a className="nav-link dropdown-toggle text-white location mt-1" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      Bengalore
                    </a>
                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                      <li><a className="dropdown-item " href="#">Chennai</a></li>
                      <li><a className="dropdown-item" href="#">Hyderabad</a></li>
                      <li><a className="dropdown-item" href="#">Delhi</a></li>
                    </ul>
                  </li>
                  <li className="nav-item mt-2">
                    <button className='sign-in px-3 py-1'>Sign in</button>
                  </li>
                  {/* <li className="nav-item">
                    <a className="nav-link" href="#">Link</a>
                  </li> */}
 
                </ul>
                <SearchModal />

              </div>
            </div>
          </nav>
          <Sections />
        </>
    );
  }
}
 
export default Navbar;