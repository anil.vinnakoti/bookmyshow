import React, { Component } from 'react'
import Bottom from './bottom';
import Contact from './contact';
import Support from './support';

class Footer extends Component {
  state = {  } 
  render() { 
    return (
      <>
      <Contact />
      <Support />
      <Bottom />
      </>
    );
  }
}
 
export default Footer;