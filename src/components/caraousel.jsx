import React, { Component } from 'react'

class Caraousel extends Component {
  state = {  } 
  render() { 
    return (
      <div id="carouselExampleInterval" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active" data-bs-interval="10000">
            <a href='#'><img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1644496573857__thehating.jpg" className="d-block w-100" alt="... /"/></a>
          </div>
          <div className="carousel-item" data-bs-interval="2000">
          <a href='#'><img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1643658635880_unlu.jpg" className="d-block w-100" alt="..." /></a>
          </div>
          <div className="carousel-item">
            <a href='#'><img src="https://assets-in.bmscdn.com/promotions/cms/creatives/1643608159306_fb.jpg" className="d-block w-100" alt="..." /></a>
          </div>
        </div>
        <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    );
  }
}
 
export default Caraousel;