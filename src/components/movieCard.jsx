import React, { Component } from 'react'

class MovieCard extends Component {
  state = {  } 
  render() { 
    const {item} = this.props
    return (
      <div className='card col-md-4 col-sm-6 flex-lg-grow-1 col-lg-2 px-0 mb-md-4 mb-3'>
        <div>
          <a href='#'><img className="card-img-top rounded-3" src={item.image}  alt="..."/></a>
        </div>
        <div className="card-body">
          <p className="card-text">{item.title}</p>
          <p className="card-text">{item.genre}</p>
        </div>
      </div>
    );
  }
}
 
export default MovieCard;