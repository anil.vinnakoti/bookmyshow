import './App.css';
import Banner1 from './components/banner1';
import BestOfEnterTainment from './components/best';
import Caraousel from './components/caraousel';
import Footer from './components/footer';
import Navbar from './components/navbar';
import Premieres from './components/premiers';
import Recommended from './components/recommended';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Caraousel />
      <Recommended />
      <Banner1 />
      <BestOfEnterTainment />
      <Premieres />
      <Footer />
    </div>
  );
}

export default App;
