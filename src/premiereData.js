export const premiereData = [
  {
    id:1,
    title:'Dune',
    language:'English',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00122526-smnpaxuurz-portrait.jpg'
  },
  {
    id:2,
    title:'Venom: Let There Be Carnage',
    language:'English',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00122532-cbfuwbccsz-portrait.jpg'
  },
  {
    id:3,
    title:'Irresistible',
    language:'English',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:oi-discovery-catalog@@icons@@premiere-icon.png,ox-322,oy-20/et00301307-hqsnyzxjvs-portrait.jpg'
  },
  {
    id:4,
    title:'Resident Evil: Welcome to Raccoon City',
    language:'English',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:oi-discovery-catalog@@icons@@premiere-icon.png,ox-322,oy-20/et00316029-sbvllbmywy-portrait.jpg'
  },
  {
    id:5,
    title:'Shane',
    language:'English',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:oi-discovery-catalog@@icons@@premiere-icon.png,ox-322,oy-20/et00318498-yqznmzrbps-portrait.jpg'
  },
]