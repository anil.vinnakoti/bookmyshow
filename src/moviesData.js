export const moviesData = [
  {
    id:1,
    title:'Khiladi',
    genre:'Action/Crime/Thriller',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@heart_202006300400.png,ox-24,oy-617,ow-29:ote-NzclICAxayB2b3Rlcw%3D%3D,ots-29,otc-FFFFFF,oy-612,ox-70/et00305648-flduhjwduc-portrait.jpg'
  },
  {
    id:2,
    title:'Love Mocktail 2',
    genre:'Drama/Romance',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@heart_202006300400.png,ox-24,oy-617,ow-29:ote-OTYlICAzMDEgdm90ZXM%3D,ots-29,otc-FFFFFF,oy-612,ox-70/et00322025-lbcskrvdar-portrait.jpg'
  },
  {
    id:3,
    title:'Badhaai DO',
    genre:'Comedy/Drama',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@like_202006280402.png,ox-24,oy-617,ow-29:ote-MTVrIGxpa2Vz,ots-29,otc-FFFFFF,oy-612,ox-70/et00128949-peluczztlf-portrait.jpg'  
  },
  {
    id:4,
    title:'FIR',
    genre:'Action/Thriller',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@like_202006280402.png,ox-24,oy-617,ow-29:ote-NmsgbGlrZXM%3D,ots-29,otc-FFFFFF,oy-612,ox-70/et00321951-zyfzkunelw-portrait.jpg'
  },
  {
    id:5,
    title:'Death on the Nile',
    genre:'Comedy/Drama/Mystery',
    image:'https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC:w-400.0,h-660.0,cm-pad_resize,bg-000000,fo-top:oi-discovery-catalog@@icons@@like_202006280402.png,ox-24,oy-617,ow-29:ote-OWsgbGlrZXM%3D,ots-29,otc-FFFFFF,oy-612,ox-70/et00119105-tlagaregqu-portrait.jpg'
  },
]